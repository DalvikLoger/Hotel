#Hotel
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as img
from matplotlib.image import imread
import seaborn as sns
sns.set_theme() # pour modifier le thème

import re
df = pd.read_csv('hotel_bookings.csv',header=0 )
db = pd.read_csv('Hotel_Reviews.csv', header=0)

Rfr = db[db['Hotel_Address'].apply(lambda row: 'France' in row)]

text = ""
for comment in Rfr.Negative_Review : 
    text += comment

# Importer stopwords de la classe nltk.corpus
from nltk.corpus import stopwords

# Initialiser la variable des mots vides
stop_words = set(stopwords.words('english'))
mots_vides = ["Negative", "would", "NegativeNo", "room", "hotel", "could", "rooms", 'also', "one", "night", 'like', "good", "pari", "stay", "get", "breakfast", "nothing", "Paris", "day"
             "really", "need", "great"]
stop_words.update(mots_vides)

from wordcloud import WordCloud
# Définir le calque du nuage des mots
wc = WordCloud(background_color="black", max_words=100, stopwords=stop_words, max_font_size=50, random_state=42)
# Générer et afficher le nuage de mots

plt.figure(figsize= (15,15))
# Initialisation d'une figure
wc.generate(text)       # "Calcul" du wordcloud 
plt.imshow(wc) # Affichage
plt.axis("off")
plt.show()