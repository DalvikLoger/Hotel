#Hotel
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as img
from matplotlib.image import imread
import seaborn as sns
sns.set_theme() # pour modifier le thème

import re
df = pd.read_csv('hotel_bookings.csv',header=0 )
db = pd.read_csv('Hotel_Reviews.csv', header=0)

Rfr = db[db['Hotel_Address'].apply(lambda row: 'France' in row)]
Rfr['Positive_Review'] = Rfr['Positive_Review'].astype('str')
Rfr['Positive_Review']

text = ''.join(Rfr['Positive_Review'][0:15000])

vocab = sorted(set(text))
print('{} uniques characters'.format(len(vocab)))

char2idx = {u:i for i, u in enumerate(vocab)}

print('{')
for char, _ in zip(char2idx, range(10)):
    print('  {:4s}: {:3d},'.format(repr(char), char2idx[char]))
print('  ...\n}')

text_as_int = [char2idx[c] for c in text]

import tensorflow as tf
seq_length = 100

# Create training examples / targets
char_dataset = tf.data.Dataset.from_tensor_slices(text_as_int)

idx2char = np.array(vocab)

for i in char_dataset.take(5):
    print(idx2char[i.numpy()])
    
sequences = char_dataset.batch(seq_length+1, drop_remainder=True)

for item in sequences.take(2):
    print(repr(''.join(idx2char[item.numpy()])))

def split_input_target(chunk):
    input_text = chunk[:-1]
    target_text = chunk[1:]
    return input_text, target_text

dataset = sequences.map(split_input_target)

for input_example, target_example in  dataset.take(1):
    print ('Input data: ', repr(''.join(idx2char[input_example.numpy()])))
    print ('Target data:', repr(''.join(idx2char[target_example.numpy()])))

    
batch_size = 64
dataset = dataset.shuffle(10000).batch(batch_size, drop_remainder=True)

from tensorflow.keras.layers import RNN, GRUCell, Dense, Embedding

# Length of the vocabulary in chars
vocab_size = len(vocab)

def build_model(batch_size):

    model = tf.keras.Sequential()

    model.add(Embedding(vocab_size, 256,
                         batch_input_shape=[batch_size, None]))

    model.add(RNN(GRUCell(512), # Cell of RNN
                return_sequences=True, # return a sequence
                stateful=True))

    model.add(Dense(vocab_size, activation='softmax'))
    
    return model

# Create model
model = build_model(64)
# Compile model
model.compile(optimizer=tf.keras.optimizers.Adam(1e-3), loss='sparse_categorical_crossentropy')
# Summary
model.summary()

model.fit(dataset, epochs=2, batch_size = 64)

def generate_text(model, start_string, num_generate = 500):
    # Converting our start string to index (vectorizing)
    input_eval = [char2idx[s] for s in start_string]
    # Simulate a batch of 1 element
    input_eval = tf.expand_dims(input_eval, 0)
    # List contains the text generated
    text_generated = []
    # Reset initial state
    model.reset_states()
    for i in range(num_generate):
        # Probability prediction
        prediction = model(input_eval)
        # Index prediction
        index = tf.argmax(prediction, axis=-1).numpy()[0]
        input_eval = tf.expand_dims([index[-1]], 0)
        # Save letter in text_generated list
        text_generated.append(idx2char[index[-1]])
    # Return all sequence
    return (start_string + ''.join(text_generated))

print(generate_text(model, start_string="hotel"))
